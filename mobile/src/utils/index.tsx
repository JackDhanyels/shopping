export { default as LoginScreen } from '../pages/Login/index';
export { default as RegisterScreen } from '../pages/Register/index';
export { default as ForgotPasswordScreen } from '../pages/Forgotpass/index';
