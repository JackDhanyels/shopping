export const theme = {
  colors: {
    surface: '#1B2732',
    primary: '#04d361',
    secondary: '#FFFFFF',
    error: '#f00000',
    buttonBorder: '#D5D5D5',
  },
};
